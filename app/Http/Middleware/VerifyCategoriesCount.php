<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class VerifyCategoriesCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Category::count() === 0) This is backend engineer ka efficiency, this will run query select count(*).
        if(Category::all()->count() === 0) // This will bring all the records and then Model ka count() use karke count dega which is not efficent. Idhar toh 50 100 records hoge but jahan millions of records hau cloud se aa rahe hai tab kya?
        {
            session()->flash('error', 'Minimum one catgeory must exists to create post!');
            return redirect(route('categories.create'));
        }
        return $next($request);
    } // next is to register the middleware
}
